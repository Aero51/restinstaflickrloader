Example Android App for loading instagram  and Flickr  photos
==================================================

Async fetching of json data with  Retrofit  callback,  async photo download using custom downloader.
Sending data between activities and fragments with greenrobot's EventBus(normal and sticky bus posting)
Used Action bar with 2 tabs (Instagram, Flickr).
Implemented master/detail view pattern.
Used ButterKnife to inject  views and listeners.





##References: 

[EventBus presentation](http://www.slideshare.net/greenrobot/eventbus-for-android-15314813)

[Productive Android: EventBus](http://awalkingcity.com/blog/2013/02/26/productive-android-eventbus/)

[How to Use the EventBus Library](http://code.tutsplus.com/tutorials/quick-tip-how-to-use-the-eventbus-library--cms-22694)

[Durable Android REST Clients](http://www.mdswanson.com/blog/2014/04/07/durable-android-rest-clients.html)

[Retrofit by Square](http://kdubblabs.com/java/retrofit-by-square/)





##Todo:
 ~~-stub~~
 
Menu item not showing when second tab is selected

Finish layouts

Implement reusing of same adapter class - 2 instances- one for instaGridfragment other for flickrGridFragment

check/implement correct behaviour/ persistance on configuration changes

implement correct back navigation when on flickr tab - detail

##Screenshot:
Note that layouts needs to be worked on.This is just for demonstration purposes.

Master detail view on phone:

![Screenshot](http://s9.postimg.org/yn8k8c2lr/slika1.png)
![Screenshot](http://s29.postimg.org/6d4pmkapz/slika2.png)


Master detail view on tablet:

![Screenshot](http://s8.postimg.org/xl9oqbjph/slika4.png)




