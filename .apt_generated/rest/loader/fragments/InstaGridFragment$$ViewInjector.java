// Generated code from Butter Knife. Do not modify!
package rest.loader.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class InstaGridFragment$$ViewInjector<T extends rest.loader.fragments.InstaGridFragment> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131099653, "field 'gridview' and method 'onItemClick'");
    target.gridview = finder.castView(view, 2131099653, "field 'gridview'");
    ((android.widget.AdapterView<?>) view).setOnItemClickListener(
      new android.widget.AdapterView.OnItemClickListener() {
        @Override public void onItemClick(
          android.widget.AdapterView<?> p0,
          android.view.View p1,
          int p2,
          long p3
        ) {
          target.onItemClick(p2);
        }
      });
  }

  @Override public void reset(T target) {
    target.gridview = null;
  }
}
