// Generated code from Butter Knife. Do not modify!
package rest.loader.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class InstaDetailFragment$$ViewInjector<T extends rest.loader.fragments.InstaDetailFragment> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131099652, "field 'description'");
    target.description = finder.castView(view, 2131099652, "field 'description'");
    view = finder.findRequiredView(source, 2131099650, "field 'picture'");
    target.picture = finder.castView(view, 2131099650, "field 'picture'");
    view = finder.findRequiredView(source, 2131099651, "field 'author'");
    target.author = finder.castView(view, 2131099651, "field 'author'");
  }

  @Override public void reset(T target) {
    target.description = null;
    target.picture = null;
    target.author = null;
  }
}
