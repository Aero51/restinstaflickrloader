package rest.loader.rest;

import rest.loader.model.FlickrPopular;
import retrofit.Callback;
import retrofit.RestAdapter;

public abstract class FlickrBaseEndpoint {
	
	 protected static final String BASE_URL = "https://api.flickr.com/services/rest";
	
    protected final RestAdapter.LogLevel logLevel;
    protected Callback<FlickrPopular> cb;
    
    
	 protected FlickrBaseEndpoint( final RestAdapter.LogLevel logLevel) {
	        this.logLevel = logLevel;
	        
	    }

}
//https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=e6dec884b7e2327c6a56684aaa22eae8&tags=gent&per_page=2&format=json&nojsoncallback=1




//https://api.flickr.com/services/rest/?method=flickr.stats.getPopularPhotos&api_key=0a3eeded0130863227cbb678746d4aa2&format=json&nojsoncallback=1