package rest.loader.rest;

import rest.loader.model.InstagramPopular;
import retrofit.Callback;
import retrofit.RestAdapter;

public abstract class InstagramBaseEndpoint {

    protected static final String BASE_URL = "https://api.instagram.com/v1";

    protected final String clientId;
    protected final RestAdapter.LogLevel logLevel;
    protected Callback<InstagramPopular> cb;

    protected InstagramBaseEndpoint(final String clientId, final RestAdapter.LogLevel logLevel) {
        this.clientId = clientId;
        this.logLevel = logLevel;
       
        
        //  private static String url = "https://api.instagram.com/v1/media/popular?client_id=e5d3b5e00f70488fa244e02e9ec49e86";
    }

}
