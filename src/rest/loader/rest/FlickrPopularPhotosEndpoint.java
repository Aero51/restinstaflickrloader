package rest.loader.rest;

import java.util.HashMap;
import java.util.Map;

import rest.loader.model.FlickrResult;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.http.GET;
import retrofit.http.QueryMap;

public class FlickrPopularPhotosEndpoint extends FlickrBaseEndpoint {

	private static Map<String, String> options = new HashMap<String, String>();

	static {
		options.put("method", "flickr.photos.getRecent");
		options.put("api_key", "8ca2bf66d4ebcce9042bb0cca81f906b");
		options.put("tags", "android");
		options.put("format", "json");
		options.put("nojsoncallback", "1");
	}

	private static interface PhotosService {
		@GET("/")
		public void getPopular(@QueryMap Map<String, String> options,
				Callback<FlickrResult> cb);

	}

	private final PhotosService photosService;

	public FlickrPopularPhotosEndpoint(final LogLevel logLevel) {
		super(logLevel);
		final RestAdapter restAdapter = new RestAdapter.Builder()
				.setLogLevel(logLevel).setEndpoint(BASE_URL).build();
		photosService = restAdapter.create(PhotosService.class);
	}

	public void getPopular(Callback<FlickrResult> cb) {
		photosService.getPopular(options, cb);
	}

}

// https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=e6dec884b7e2327c6a56684aaa22eae8&tags=android&per_page=20&format=json&nojsoncallback=1

// https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=e6dec884b7e2327c6a56684aaa22eae8&per_page=20&format=json&nojsoncallback=1

// https://api.flickr.com/services/rest/?method=flickr.stats.getPopularPhotos&api_key=8ca2bf66d4ebcce9042bb0cca81f906b&format=json&nojsoncallback=1