package rest.loader.rest;

import rest.loader.model.InstagramPopular;

import rest.loader.rest.InstagramBaseEndpoint;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;

public class InstagramMediaEndpoint extends InstagramBaseEndpoint {

	private static interface MediaService {

		@GET("/media/popular")
		public void getPopular(@Query("client_id") String clientId,
				Callback<InstagramPopular> cb);

	}

	private final MediaService mediaService;

	public InstagramMediaEndpoint(final String clientId,
			final RestAdapter.LogLevel logLevel) {
		super(clientId, logLevel);
		final RestAdapter restAdapter = new RestAdapter.Builder()
				.setLogLevel(logLevel).setEndpoint(BASE_URL).build();
		mediaService = restAdapter.create(MediaService.class);
	}

	public void getPopular(Callback<InstagramPopular> cb) {
		mediaService.getPopular(clientId, cb);
	}

}
// private static String url =
// "https://api.instagram.com/v1/media/popular?client_id=e5d3b5e00f70488fa244e02e9ec49e86";

