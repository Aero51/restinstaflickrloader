package rest.loader.rest;

public enum InstagramScope {

    basic, comments, relationships, likes

}
