package rest.loader.activities;

import rest.loader.R;
import rest.loader.fragments.FlickrDetailFragment;
import rest.loader.fragments.InstaDetailFragment;
import android.app.Activity;
import android.os.Bundle;

public class FlickrDetailActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail_activity);
		
		 if (savedInstanceState == null) {
			 FlickrDetailFragment detail_fragment= new FlickrDetailFragment();
			 getFragmentManager().beginTransaction()
             .add(R.id.item_detail_container, detail_fragment)
             .commit();
		 }
	}
}
