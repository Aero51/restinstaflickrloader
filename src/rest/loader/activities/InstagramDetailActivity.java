package rest.loader.activities;

import rest.loader.R;
import rest.loader.fragments.InstaDetailFragment;
import android.app.Activity;
import android.os.Bundle;

public class InstagramDetailActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail_activity);
		
		 if (savedInstanceState == null) {
			 InstaDetailFragment detail_fragment= new InstaDetailFragment();
			 getFragmentManager().beginTransaction()
             .add(R.id.item_detail_container, detail_fragment)
             .commit();
		 }
	}

}
