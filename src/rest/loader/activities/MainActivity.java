package rest.loader.activities;

import de.greenrobot.event.EventBus;
import rest.loader.R;
import rest.loader.events.FlickrItemSelectedEvent;
import rest.loader.events.InstagramItemSelectedEvent;
import rest.loader.fragments.FlickrDetailFragment;
import rest.loader.fragments.FlickrGridFragment;
import rest.loader.fragments.InstaDetailFragment;
import rest.loader.fragments.InstaGridFragment;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends Activity {

	public void onEvent(InstagramItemSelectedEvent event) {

		boolean mTwoPane = findViewById(R.id.item_detail_container) != null;
		if (mTwoPane) {
			InstaDetailFragment fragment = new InstaDetailFragment();
			getFragmentManager().beginTransaction()
					.replace(R.id.item_detail_container, fragment).commit();

		} else {

			Intent instagramDetailIntent = new Intent(this,
					InstagramDetailActivity.class);
			startActivity(instagramDetailIntent);
		}
	}

	public void onEvent(FlickrItemSelectedEvent event) {
		boolean mTwoPane = findViewById(R.id.item_detail_container) != null;
		if (mTwoPane) {
		FlickrDetailFragment fragment = new FlickrDetailFragment();
		getFragmentManager().beginTransaction()
		.replace(R.id.item_detail_container, fragment).commit();
		}
		else {
			Intent flickrDetailIntent = new Intent(this, FlickrDetailActivity.class);
			startActivity(flickrDetailIntent);
		}
		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_action_bar_main);

		EventBus.getDefault().register(this);

		ActionBar actionbar = getActionBar();
		// actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		Tab tab = actionbar.newTab();
		tab.setText("Instagram");

		TabListener<InstaGridFragment> t1 = new TabListener<InstaGridFragment>(
				this, "Instagram", InstaGridFragment.class);

		tab.setTabListener(t1);
		actionbar.addTab(tab);

		tab = actionbar.newTab();
		tab.setText("Flickr");
		TabListener<FlickrGridFragment> t2 = new TabListener<FlickrGridFragment>(
				this, "Flickr", FlickrGridFragment.class);
		tab.setTabListener(t2);
		actionbar.addTab(tab);

		if (savedInstanceState != null) {
			actionbar.setSelectedNavigationItem(savedInstanceState.getInt(
					"tab", 0));

		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt("tab", getActionBar().getSelectedNavigationIndex());
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onPause() {
		EventBus.getDefault().unregister(this);
		super.onPause();
	}

	private class TabListener<T extends Fragment> implements
			ActionBar.TabListener {

		private final Activity mActivity;
		private final String mTag;
		private final Class<T> mClass;
		private final Bundle mArgs;
		private Fragment mFragment;

		public TabListener(Activity activity, String tag, Class<T> clz) {
			this(activity, tag, clz, null);
		}

		public TabListener(Activity activity, String tag, Class<T> clz,
				Bundle args) {
			mActivity = activity;
			mTag = tag;
			mClass = clz;
			mArgs = args;

			mFragment = mActivity.getFragmentManager().findFragmentByTag(mTag);
			if (mFragment != null && !mFragment.isDetached()) {
				FragmentTransaction ft = mActivity.getFragmentManager()
						.beginTransaction();
				ft.detach(mFragment);
				ft.commit();
			}
		}

		@Override
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			if (mFragment == null) {
				mFragment = Fragment.instantiate(mActivity, mClass.getName(),
						mArgs);
				ft.add(R.id.mainLayout, mFragment, mTag);
			} else {
				ft.attach(mFragment);
			}
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			if (mFragment != null) {
				// Detach the fragment, because another one is being attached
				ft.detach(mFragment);
			}
		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction ft) {
			Toast.makeText(mActivity, "Reselected!", Toast.LENGTH_SHORT).show();
		}
	}

}
