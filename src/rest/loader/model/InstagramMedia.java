package rest.loader.model;



public class InstagramMedia {

	private InstagramLocation location;
    private InstagramImages images;

    private InstagramCaption caption;

    private InstagramUser user;

 
    public InstagramImages getImages() {
        return images;
    }

 
    public InstagramCaption getCaption() {
        return caption;
    }

    public InstagramUser getUser() {
        return user;
    }

    public InstagramLocation getLocation() {
        return location;
    }
    
}
