package rest.loader.model;

public class InstagramCaption {

    private Long created_time;
    private String text;
    private InstagramUser from;
    private Long id;

    public Long getCreatedTime() {
        return created_time;
    }

    public String getText() {
        return text;
    }

    public InstagramUser getFrom() {
        return from;
    }

    public Long getId() {
        return id;
    }

}
