package rest.loader.model;

public class FlickrPhoto {
	
	private String id;
	private String owner;
	private String secret;
	private String server;
	private int farm;
	private String title;
	private int ispublic;
	private int isfriend;
	private int isfamily;
	
	private String thumbnailUrl;
	

	private String imageUrl;
	
	
	
	public String getId() {
		return id;
	}
	public String getOwner() {
		return owner;
	}
	public String getSecret() {
		return secret;
	}
	public String getServer() {
		return server;
	}
	public int getFarm() {
		return farm;
	}
	public String getTitle() {
		return title;
	}
	public int getIspublic() {
		return ispublic;
	}
	public int getIsfriend() {
		return isfriend;
	}
	public int getIsfamily() {
		return isfamily;
	}
	public String getThumbnailUrl() {
		return thumbnailUrl;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}
	
	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	
}
