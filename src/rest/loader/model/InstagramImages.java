package rest.loader.model;

public class InstagramImages {

    private InstagramImage low_resolution;
    private InstagramImage thumbnail;
    private InstagramImage standard_resolution;

    public InstagramImage getLowResolution() {
        return low_resolution;
    }

    public InstagramImage getThumbnail() {
        return thumbnail;
    }

    public InstagramImage getStandardResolution() {
        return standard_resolution;
    }

}
