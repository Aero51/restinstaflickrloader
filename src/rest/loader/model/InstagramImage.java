package rest.loader.model;

public class InstagramImage {

    private String url;
    private Integer width;
    private Integer height;

    public String getUrl() {
        return url;
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getHeight() {
        return height;
    }

}
