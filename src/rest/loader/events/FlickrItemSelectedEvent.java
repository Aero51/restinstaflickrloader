package rest.loader.events;

public class FlickrItemSelectedEvent {
	public FlickrItemSelectedEvent(int position) {
	
		this.position = position;
	}

	public final int position;
}
