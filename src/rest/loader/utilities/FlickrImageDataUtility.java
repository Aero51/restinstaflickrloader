package rest.loader.utilities;

import java.util.List;

import rest.loader.model.FlickrPhoto;



public class FlickrImageDataUtility {
	
	public static List<FlickrPhoto> getImageUrls( List<FlickrPhoto> photo_list){
		
		
		for(FlickrPhoto pic : photo_list){
			
			String thumbnailUrl=FlickrPictureUrlUtility.getFlickrThumbnailUrl(pic);
			String imageUrl = FlickrPictureUrlUtility.getFlickrLargeUrl(pic);
			
			pic.setThumbnailUrl(thumbnailUrl);
			pic.setImageUrl(imageUrl);
			
		}
		
		
		
		
		return photo_list;
	
		
		
	}
	

}
