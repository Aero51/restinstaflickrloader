package rest.loader.utilities;

import rest.loader.model.FlickrPhoto;



public class FlickrPictureUrlUtility {

	// "http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg";
	
		private static String FLICKR_BASE_URL = "http://farm%d.staticflickr.com/%s/%s_%s_%s.jpg";
		
		public static String getFlickrThumbnailUrl(FlickrPhoto flickrPhoto){
			String url = String.format(FLICKR_BASE_URL, flickrPhoto.getFarm(), flickrPhoto.getServer(), flickrPhoto.getId(), flickrPhoto.getSecret(), "t");
			return url;
		}
		
		public static String getFlickrLargeUrl(FlickrPhoto flickrPhoto){
			String url = String.format(FLICKR_BASE_URL, flickrPhoto.getFarm(), flickrPhoto.getServer(), flickrPhoto.getId(), flickrPhoto.getSecret(), "b");
			return url;
		}
	
	
	
}
