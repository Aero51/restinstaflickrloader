package rest.loader.fragments;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import rest.loader.R;
import rest.loader.adapters.FlickrAdapterGrid;
import rest.loader.events.FlickrItemSelectedEvent;
import rest.loader.events.InstagramItemSelectedEvent;
import rest.loader.model.FlickrPhoto;
import rest.loader.model.FlickrResult;
import rest.loader.model.InstagramMedia;
import rest.loader.rest.FlickrPopularPhotosEndpoint;
import rest.loader.utilities.FlickrImageDataUtility;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

public class FlickrGridFragment extends Fragment implements OnItemClickListener {

//	@InjectView(R.id.mainGrid)GridView gridview;

	private static List<FlickrPhoto> photo_list = new ArrayList<FlickrPhoto>();
	private static FlickrAdapterGrid adapter;
	private static Parcelable listViewState;
	private static MenuItem menuItem;

	private FlickrPopularPhotosEndpoint ePhotosEndpoint;

	/*@OnItemClick(R.id.mainGrid)
	void onItemClick(int position) {
		FlickrPhoto photo = photo_list.get(position);
		EventBus.getDefault().postSticky(photo);
		EventBus.getDefault().post(new FlickrItemSelectedEvent(position));
	}
*/
	private Callback<FlickrResult> cb = new Callback<FlickrResult>() {
		@Override
		public void success(FlickrResult arg0, Response arg1) {

			List<FlickrPhoto> local_list = new ArrayList<FlickrPhoto>();
			local_list = arg0.photos.getPhotoList();
			local_list = FlickrImageDataUtility.getImageUrls(local_list);
			photo_list.clear();
			photo_list.addAll(local_list);
			adapter.notifyDataSetChanged();
			if (menuItem != null) {
				menuItem.collapseActionView();
				menuItem.setActionView(null);
			}
		}

		@Override
		public void failure(RetrofitError arg0) {
			Log.d("CallbackFlickr",
					"Dogodio se retrofit error: " + arg0.getMessage());
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d("Cycles", "Flickr Fragment onCreate ");
		setHasOptionsMenu(true);

		ePhotosEndpoint = new FlickrPopularPhotosEndpoint(
				RestAdapter.LogLevel.NONE);
		ePhotosEndpoint.getPopular(cb);
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onAttach(Activity activity) {
		Log.d("Cycles", "Flickr Fragment onAttach ");
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.gridview_activity, container,
				false);
		//ButterKnife.inject(this, view);
		GridView gridview=(GridView) view.findViewById(R.id.mainGrid);
		gridview.setOnItemClickListener(this);
		if (adapter == null) {
			adapter = new FlickrAdapterGrid(getActivity(), photo_list);

		} else {
			gridview.setVisibility(View.VISIBLE);
		}

		gridview.setAdapter(adapter);

		Log.d("Cycles", "Flickr Fragment onCreateView  ");
		return view;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menumain, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.refresh:
			Toast.makeText(getActivity(), "Refresh was clicked.",
					Toast.LENGTH_SHORT).show();
			menuItem = item;
			menuItem.setActionView(R.layout.progressbar);
			menuItem.expandActionView();
			ePhotosEndpoint.getPopular(cb);

			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d("Cycles", "Flickr Fragment onActivityCreated ");
		super.onActivityCreated(savedInstanceState);
		setRetainInstance(true);
	}

	@Override
	public void onStart() {
		Log.d("Cycles", "Flickr Fragment onStart ");
		super.onStart();
	}

	@Override
	public void onResume() {
		Log.d("Cycles", "Flickr Fragment onResume ");
		super.onResume();
	}

	@Override
	public void onPause() {
		Log.d("Cycles", "Flickr Fragment onPause ");
		super.onPause();
	}

	@Override
	public void onDestroyView() {
		Log.d("Cycles", "Flickr Fragment onDestroyView ");
		super.onDestroyView();
	}

	@Override
	public void onDetach() {
		Log.d("Cycles", "Flickr Fragment onDetach ");
		super.onDetach();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		FlickrPhoto photo = photo_list.get(position);
		EventBus.getDefault().postSticky(photo);
		EventBus.getDefault().post(new FlickrItemSelectedEvent(position));
	}

}
