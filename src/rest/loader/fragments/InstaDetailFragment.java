package rest.loader.fragments;

import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

import rest.loader.R;
import rest.loader.activities.InstagramDetailActivity;
import rest.loader.events.InstagramItemSelectedEvent;
import rest.loader.model.InstagramMedia;
import rest.loader.rest.DrawableBackgroundDownloader;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class InstaDetailFragment extends Fragment {

	/*@InjectView(R.id.picture)
	ImageView picture;
	@InjectView(R.id.author)
	TextView author;
	@InjectView(R.id.description)
	TextView description;
	*/

	private DrawableBackgroundDownloader downloader = new DrawableBackgroundDownloader();

	public void onEvent(InstagramMedia event) {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.detail_fragment, container,
				false);

		//ButterKnife.inject(this, rootView);
		ImageView picture=(ImageView)rootView.findViewById(R.id.picture);
		TextView author=(TextView)rootView.findViewById(R.id.author);
		TextView description=(TextView)rootView.findViewById(R.id.description);
		
		InstagramMedia media = (InstagramMedia) EventBus.getDefault()
				.getStickyEvent(InstagramMedia.class);

		author.setText(media.getUser().getUsername());
		description.setText(media.getCaption().getText());

		Drawable drawable = rootView.getResources().getDrawable(
				R.drawable.no_image);
		downloader.loadDrawable(media.getImages().getStandardResolution()
				.getUrl(), picture, drawable);
		return rootView;
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroyView() {
		//ButterKnife.reset(this);
		super.onDestroyView();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

}
