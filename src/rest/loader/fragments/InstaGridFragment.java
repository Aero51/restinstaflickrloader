package rest.loader.fragments;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import de.greenrobot.event.EventBus;
import rest.loader.R;
import rest.loader.adapters.InstagramAdapterGrid;
import rest.loader.events.InstagramItemSelectedEvent;
import rest.loader.model.InstagramMedia;
import rest.loader.model.InstagramPopular;
import rest.loader.rest.InstagramMediaEndpoint;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

public class InstaGridFragment extends Fragment implements OnItemClickListener {
	//@Bind(R.id.mainGrid)GridView gridview;

	private static String client_id = "e5d3b5e00f70488fa244e02e9ec49e86";
	private static List<InstagramMedia> list = new ArrayList<InstagramMedia>();;

	private static InstagramAdapterGrid adapter;

	private static Parcelable listViewState;
	private static MenuItem menuItem;

	private InstagramMediaEndpoint eMediaEndpoint;

	/*@OnItemClick(R.id.mainGrid)
	void onItemClick(int position) {
		InstagramMedia media_object = list.get(position);
		EventBus.getDefault().postSticky(media_object);
		EventBus.getDefault().post(new InstagramItemSelectedEvent(position));
	}*/

	private Callback<InstagramPopular> cb = new Callback<InstagramPopular>() {
		@Override
		public void success(InstagramPopular arg0, Response arg1) {
			List<InstagramMedia> lista = arg0.getMediaList();
			list.clear();
			list.addAll(lista);
			adapter.notifyDataSetChanged();
				if (menuItem != null) {
					menuItem.collapseActionView();
					menuItem.setActionView(null);
				}
		}

		@Override
		public void failure(RetrofitError arg0) {
			Log.d("Callback", "Dogodio se retrofit error: " + arg0.getMessage());
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.gridview_activity, container,
				false);
	//	ButterKnife.inject(this, view);
		//ButterKnife.bind(this,view);
		GridView gridview=(GridView) view.findViewById(R.id.mainGrid);
		gridview.setOnItemClickListener(this);
		
		if (adapter == null) {
			adapter = new InstagramAdapterGrid(getActivity(), list);

		} else {
			gridview.setVisibility(View.VISIBLE);
		}

		gridview.setAdapter(adapter);

		return view;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d("Cycles", "Insta Fragment onCreate ");

		setHasOptionsMenu(true);
		setRetainInstance(true);

		eMediaEndpoint = new InstagramMediaEndpoint(client_id,
				RestAdapter.LogLevel.NONE);
		eMediaEndpoint.getPopular(cb);

		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d("Cycles", "Insta Fragment onActivityCreated ");
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume() {
		if (listViewState != null) {
		}
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menumain, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.refresh:
			Toast.makeText(getActivity(), "Refresh was clicked.",
					Toast.LENGTH_SHORT).show();
			menuItem = item;
			menuItem.setActionView(R.layout.progressbar);
			menuItem.expandActionView();
			eMediaEndpoint.getPopular(cb);

			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		InstagramMedia media_object = list.get(position);
		EventBus.getDefault().postSticky(media_object);
		EventBus.getDefault().post(new InstagramItemSelectedEvent(position));
	}

	

}
