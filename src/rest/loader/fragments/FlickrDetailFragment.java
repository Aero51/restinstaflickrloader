package rest.loader.fragments;

import de.greenrobot.event.EventBus;
import rest.loader.R;
import rest.loader.model.FlickrPhoto;
import rest.loader.model.InstagramMedia;
import rest.loader.rest.DrawableBackgroundDownloader;
import butterknife.ButterKnife;
import android.app.Fragment;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class FlickrDetailFragment extends Fragment {

/*	@InjectView(R.id.picture)
	ImageView picture;
	@InjectView(R.id.author)
	TextView author;
	@InjectView(R.id.description)
	TextView description;
*/
	
	private DrawableBackgroundDownloader downloader = new DrawableBackgroundDownloader();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.detail_fragment, container,
				false);

	//	ButterKnife.inject(this, rootView);
		FlickrPhoto photo = (FlickrPhoto) EventBus.getDefault().getStickyEvent(
				FlickrPhoto.class);
		
		ImageView picture=(ImageView)rootView.findViewById(R.id.picture);
		TextView author=(TextView)rootView.findViewById(R.id.author);
		TextView description=(TextView)rootView.findViewById(R.id.description);
		
		author.setText(photo.getOwner());
		description.setText(photo.getTitle());
		Drawable drawable = rootView.getResources().getDrawable(
				R.drawable.no_image);
		downloader.loadDrawable(photo.getImageUrl(), picture, drawable);
		return rootView;
	}

}
