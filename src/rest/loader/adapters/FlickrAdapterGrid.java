package rest.loader.adapters;

import java.util.ArrayList;
import java.util.List;

import rest.loader.R;
import rest.loader.adapters.InstagramAdapterGrid.ViewHolder;
import rest.loader.model.FlickrPhoto;
import rest.loader.model.InstagramMedia;
import rest.loader.rest.DrawableBackgroundDownloader;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class FlickrAdapterGrid extends BaseAdapter {
	private Context mContext;
	private DrawableBackgroundDownloader downloader = new DrawableBackgroundDownloader();
	private List<FlickrPhoto> aList = new ArrayList<FlickrPhoto>();

	public FlickrAdapterGrid(Context c, List<FlickrPhoto> list) {
		mContext = c;
		aList = list;
	}


	@Override
	public int getCount() {
		return aList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			holder.picture = new ImageView(mContext);
			holder.picture.setScaleType(ImageView.ScaleType.CENTER_CROP);
			holder.picture.setLayoutParams(new GridView.LayoutParams(150, 150));
			convertView = holder.picture;
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		Drawable drawable = mContext.getResources().getDrawable(
				R.drawable.no_image);

		downloader.loadDrawable(aList.get(position).getThumbnailUrl(),
				holder.picture, drawable);

		return convertView;

	}

}
