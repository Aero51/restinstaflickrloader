package rest.loader.adapters;

import java.util.ArrayList;
import java.util.List;

import rest.loader.R;

import rest.loader.model.InstagramMedia;
import rest.loader.rest.DrawableBackgroundDownloader;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class InstagramAdapterGrid extends BaseAdapter {

	private Context mContext;
	private DrawableBackgroundDownloader downloader = new DrawableBackgroundDownloader();
	private List<InstagramMedia> aList = new ArrayList<InstagramMedia>();

	public InstagramAdapterGrid(Context c, List<InstagramMedia> list) {
		mContext = c;
		aList = list;
	}

	

	@Override
	public int getCount() {

		return aList.size();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ViewHolder holder;
		if (view == null) {
			holder = new ViewHolder();
			holder.picture = new ImageView(mContext);
			holder.picture.setScaleType(ImageView.ScaleType.CENTER_CROP);
			holder.picture.setLayoutParams(new GridView.LayoutParams(150, 150));
			view = holder.picture;
			view.setTag(holder);

		} else {
			holder = (ViewHolder) view.getTag();
		}

		Drawable drawable = mContext.getResources().getDrawable(
				R.drawable.no_image);

		downloader.loadDrawable(aList.get(position).getImages().getThumbnail()
				.getUrl(), holder.picture, drawable);

		return view;
	}

	static class ViewHolder {
		ImageView picture;
		String author;
		String description;
	}
}
